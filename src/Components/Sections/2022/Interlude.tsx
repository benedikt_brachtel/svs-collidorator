import styled from "@emotion/styled";
import React from "react";
import backgroundImage from "../../../images/2022/SectionInterludeBackground.png";
import flowershape from "../../../images/2022/SectionInterludeFlowerShape.svg";
import shrimp from "../../../images/2022/SectionInterludeShrimp.png";
import truehorsi from "../../../images/2022/SectionInterludeTrueHorsi.svg";
import NarrowSection from "../../Section/NarrowSection";

const Container = styled.div`
  background-image: url(${backgroundImage});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  /* padding: 10rem 0; */
  position: relative;
  .flowershape {
    bottom: 0;
    position: absolute;
    left: 0;
    right: 0;
  }
  img {
    width: 100%;
  }
`;

const Interlude = () => {
  return (
    <Container>
      <img src={truehorsi} className="truehorsi" alt="true horsi" />
      <NarrowSection>
        <img src={shrimp} className="shrimp" alt="shrimp" />
      </NarrowSection>
      <img src={flowershape} className="flowershape" alt="true horsi" />
    </Container>
  );
};

export default Interlude;
