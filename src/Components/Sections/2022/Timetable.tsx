import styled from "@emotion/styled";
import React from "react";
import { device } from "../../../breakpoints";
import backgroundImage from "../../../images/2022/SectionTimetableBackground.png";
import mountrushmore from "../../../images/2022/SectionTimetableMountRushmore.svg";
import NarrowSection from "../../Section/NarrowSection";

const Container = styled.div`
  background-image: url(${backgroundImage});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  font-size: 3rem;
  line-height: 3rem;
  font-weight: normal;
  color: var(--festival-primary);
  padding: 10rem 0;
  img {
    width: 100%;
  }
  @media ${device.mdDown} {
    font-size: 2rem;
    line-height: 2rem;
  }
  h1 {
    font-size: 4rem;
    line-height: 4rem;
    margin: 3rem 0;
    text-align: center;
    font-weight: normal;
    text-transform: uppercase;
    color: var(--festival-primary);
    @media ${device.mdDown} {
      font-size: 2rem;
      line-height: 2rem;
    }
  }
  section {
    h3 {
      margin: 3rem 0;
      font-size: 3rem;
      line-height: 3rem;
      font-weight: bold;
      color: var(--festival-primary);
      @media ${device.mdDown} {
        font-size: 2rem;
        line-height: 2rem;
      }
    }
    h2 {
      position: sticky;
      margin: 3rem 0;
      font-size: 3rem;
      line-height: 3rem;
      top: 2rem;
      color: var(--festival-primary);
      @media ${device.mdDown} {
        font-size: 2rem;
        line-height: 2rem;
      }
    }
  }
`;

const Timetable = () => {
  return (
    <Container>
      <h1>
        SVS COLLISIONS EXHIBITION
      </h1>
      <NarrowSection>
        <h2>Tuesday, 19 July to Thursday, 21 July 2022<br />
        Saturday, 23 July &amp; Sunday, 24 July 2022</h2>
        <h3>Location: Studio 1</h3>
        16:00-23:00 MONOM STAGE<br />
        <h3>Location: Studio 2</h3>
        16:00-23:00 INSTALLATIONS
      </NarrowSection>
      <h1>
        COLLISIONS FESTIVAL
        <br />
        <a href="https://www.eventim.de/eventseries/collisions-festival-3158614/?affiliate=GMD" target="_blank"><U>TICKETS</U></a>
        <br />
        LINEUP AND TIMETABLE
      </h1>
      <NarrowSection>
        <h2>Monday, 18 July 2022</h2>
        <h3>Location: Muffat Café</h3>
        16:00-18:00 Radio 80000 LIVE from Muffat Café
      </NarrowSection>
      <NarrowSection>
        <h2>Tuesday, 19 July 2022</h2>
        <h3>Location: Muffat Café</h3>
        16:00 Radio 80000 LIVE from Muffat Café
        <br />
        18:00 "Café &amp; Kosmos" – Talks series by Max-Planck-Institute
        <br />
        <br />
        <h3>Location: Ampere</h3>
        20:00 Rosi96, David Goldberg (Livecoding)
        <br />
        21:00 Heinrich Schwarzer (Liveset)
        <br />
        21:30 Ferdinand Domes (Liveset)
        <br />
        22:00 ChaosAngel (DJ Set)
        <br />
        23:30 Daniel Door (Liveset)
        <br />
        24:00 Zoe McPherson (Liveset)
        <br />
        01:00 Ghosttown (DJ Set)
      </NarrowSection>
      <NarrowSection>
        <h2>Wednesday, 20 July 2022</h2>
        <br />
        Location: Muffat Café
        <br />
        16:00-18:00 Radio 80000 LIVE from Muffat Café
        <br />
        <br />
        <h3>Location: Muffathalle</h3>
        19:00–23:00 Anonima Luci / Katatonic Silentio
        <br />
        <br />
        <h3>Location: Ampere</h3>
        20:00 Specimens (Liveset)
        <br />
        21:00 Aloïs Yang (Liveset)
        <br />
        22:00 Lybes Dimem (AV Liveset)
        <br />
        23:00 Elsa M´Bala (Liveset)
        <br />
        00:00 Kamron Saniee (Liveset)
        <br />
        00:30 Jessica Ekomane (Liveset)
        <br />
        01:00 Lovefingers (DJ Set)      
      </NarrowSection>
      <NarrowSection>
        <h2>Thursday, 21 July 2022</h2>
        <br />
        <h3>Location: Muffathalle</h3>
        19:00–23:00 Anonima Luci / Katatonic Silentio
        <br />
        <br />
        <h3>Location: Muffat Café</h3>
        16:00-18:00 Radio 80000 LIVE from Muffat Café
        <br />
        21:00 Cosmica Bandida
        <br />
        22:00 Mandy Mozart
        <br />
        23:00 Bartellow &amp; Sven Michelson Live
        <br />
        00:00 Slovva
        <br />
        01:00 ojoo gyal
        <br />
        03:00 Rosi96 &amp; David Goldberg
        <br />
        04:00 Konrad Wehrmeister
      </NarrowSection>
      <img src={mountrushmore} className="mountrushmore" alt="mounta rushmore"/>
    </Container>
  );
};

export default Timetable;
