import styled from "@emotion/styled";
import React, { FC, ReactNode } from "react";
import { device } from "../../breakpoints";

const Container = styled.section`
  width: var(--inner-width);
  margin: 0 auto;
  text-align: center;
  @media ${device.lgDown} {
    width: calc(100% - 2rem);
    padding: 0 1rem;
  }
`;

type Props = {
  children: ReactNode | ReactNode[];
};

const Section: FC<Props> = ({children }) => {
  return <Container>{children}</Container>;
};

export default Section;
