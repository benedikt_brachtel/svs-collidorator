import styled from "@emotion/styled";
import React from "react";
import Residency from "./Components/Sections/2021/Residency";
import Festival from "./Components/Sections/2022/Festival";
import LayoutLogic from "./LayoutLogic";

const Container = styled.main``;

function App() {
  return (
    <Container>
      <LayoutLogic />
      <Festival/>
      <Residency/>
    </Container>
  );
}

export default App;
